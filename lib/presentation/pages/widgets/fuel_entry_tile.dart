import 'package:flutter/material.dart';
import 'package:old_car_meter/data/models/fuel_entry.dart';

class FuelEntryTile extends StatelessWidget {
  final FuelEntry fuelEntry;

  const FuelEntryTile({
    Key key,
    @required this.fuelEntry,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: fuelEntry.consumption() == null
            ? Icon(Icons.add_road)
            : Text(
                '${fuelEntry.consumption()}',
                style: TextStyle(fontSize: 25),
              ),
        title: Text(
            '${fuelEntry.totalDistance} km    ${fuelEntry.distanceSinceLastRefueld()} km    ${fuelEntry.amountRefueld} L'),
        subtitle: Align(
            alignment: Alignment.centerRight,
            child: Text('${fuelEntry.amountRefueld} L     ${fuelEntry.dateTimeString()}')),
      ),
    );
  }
}
