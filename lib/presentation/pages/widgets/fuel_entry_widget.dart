import 'package:flutter/material.dart';
import 'package:old_car_meter/data/models/fuel_entry.dart';

class FuelEntryWidget extends StatelessWidget {
  final FuelEntry fuelEntry;

  const FuelEntryWidget({
    Key key,
    @required this.fuelEntry,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          fuelEntry.consumption() == null
              ? Text('')
              : Text('${fuelEntry.consumption()}')
        ],
      ),
      Column(
        children: [
          Text('${fuelEntry.totalDistance}'),
          Text('${fuelEntry.distanceSinceLastRefueld()}')
        ],
      ),
      Column(
        children: [
          Text('${fuelEntry.dateTimeString()}'),
          Text('${fuelEntry.amountRefueld}')
        ],
      ),
    ]);
  }
}
