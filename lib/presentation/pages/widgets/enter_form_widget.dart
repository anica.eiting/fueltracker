import 'package:flutter/material.dart';

class EnterFormWidget extends StatefulWidget {
  // final FilterArguments filterArguments;

  const EnterFormWidget({
    Key key,
    // @required this.filterArguments,
  }) : super(key: key);

  @override
  EnterFormWidgetState createState() => EnterFormWidgetState();
}

class EnterFormWidgetState extends State<EnterFormWidget> {
  final _formKey = GlobalKey<FormState>();

  bool showFuelForm = false;
  bool showKmForm = false;
  Color activeColor = Color(0xFF92D050);
  Color inActiveColor = Color(0xFF6EAA2E);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FlatButton(
              onPressed: () {
                setState(() {
                  showFuelForm = !showFuelForm;
                });
              },
              child: Icon(Icons.local_gas_station, color: Colors.white),
              color: showFuelForm ? activeColor : inActiveColor),
          FlatButton(
              onPressed: () {},
              child: Icon(Icons.add_road, color: Colors.white),
              color: showKmForm ? activeColor : inActiveColor),
        ],
      ),
      if (showFuelForm)
        Card(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 4, horizontal: 4),
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                child: Row(children: [
                  // Icon(Icons.local_gas_station),
                  // SizedBox(width: 16),
                  Expanded(
                      child: Form(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            // hintText: 'Wieviel Liter hast du getankt?',
                            labelText: 'Liter',
                            icon: Icon(Icons.local_gas_station),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Bitte Anzahl Liter eingeben';
                            } else {
                              value = value.replaceAll(',', '.');
                              double fuelAmount = double.tryParse(value);
                              if (fuelAmount == null) {
                                return 'Keine gültige Kommazahl';
                              }
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            // hintText: 'Wie ist der aktuelle km Stand?',
                            labelText: 'aktueller Kilometerstand',
                            icon: Icon(Icons.add_road),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Bitte Anzahl km eingeben';
                            } else {
                              value = value.replaceAll(',', '.');
                              double fuelAmount = double.tryParse(value);
                              if (fuelAmount == null) {
                                return 'Keine gültige Kommazahl';
                              }
                            }
                            return null;
                          },
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 0),
                          child: Row(
                            children: [
                              IconButton(
                                color: inActiveColor,
                                onPressed: () {
                                  // Validate will return true if the form is valid, or false if
                                  // the form is invalid.
                                  if (_formKey.currentState.validate()) {
                                    // Process data.
                                  }
                                },
                                icon: Icon(Icons.calculate),
                              ),
                              Text('oder'),
                              Text(' gefahrene Strecke')
                            ],
                          ),
                        ),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 0.0),
                              child: FlatButton(
                                color: inActiveColor,
                                onPressed: () {
                                  // Validate will return true if the form is valid, or false if
                                  // the form is invalid.
                                  if (_formKey.currentState.validate()) {
                                    // Process data.
                                  }
                                },
                                child: Icon(Icons.check, color: Colors.white),
                              ),
                            )),
                      ],
                    ),
                  ))
                ])))
    ]));
  }
}
