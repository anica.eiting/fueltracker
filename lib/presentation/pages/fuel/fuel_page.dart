import 'package:flutter/material.dart';
import 'package:old_car_meter/data/models/fuel_entry.dart';
import 'package:old_car_meter/presentation/pages/widgets/enter_form_widget.dart';
import 'package:old_car_meter/presentation/pages/widgets/fuel_entry_tile.dart';
import 'package:old_car_meter/presentation/pages/widgets/fuel_entry_widget.dart';

class FuelPage extends StatelessWidget {
  static const String routeName = 'fuel';

  const FuelPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<FuelEntry> fuelEntries = createTestData();

    return Scaffold(
        appBar: AppBar(
          title: Center(
              child: Text(
            'Fuel Tracker',
            style: TextStyle(fontSize: 16),
          )),
          toolbarHeight: 30,
        ),
        body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomRight,
                end: Alignment.topLeft,
                colors: [
                  const Color(0xFF92D050),
                  const Color(0xFF9FEDD7)
                  // const Color(0xFFFEF9C7),
                  // const Color(0xFFBA9204)
                  // Color(0xFF026670)
                  //0xFF4A3D31
                ], // red to yellow
                tileMode: TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
            child: Column(children: [
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 0),
                  child: Center(
                      child: Text(
                    '⌀ ' + getMeanFuelConsumption(fuelEntries).toString(),
                    style: TextStyle(fontSize: 25),
                  ))),
              EnterFormWidget(),
              Expanded(
                child: ListView.builder(
                  //padding: const EdgeInsets.all(16),
                  shrinkWrap: true,
                  itemCount: fuelEntries.length,
                  itemBuilder: (BuildContext context, int index) {
                    final fuelEntry = fuelEntries[index];
                    return FuelEntryTile(fuelEntry: fuelEntry);
                  },
                ),
              ),
            ])));
  }

  List<FuelEntry> createTestData() {
    List<FuelEntry> fuelEntries = [];
    for (int i = 0; i < 10; i++) {
      fuelEntries.add(createFuelEntry());
    }
    FuelEntry fe1 = new FuelEntry();
    fe1.amountRefueld = 35.5;
    fe1.totalDistance = 123456;
    fe1.dateTime = new DateTime(2017);
    fuelEntries.add(fe1);
    FuelEntry fe2 = new FuelEntry();
    fe2.amountRefueld = 40.2;
    fe2.totalDistance = 123988;
    fe2.dateTime = new DateTime(2018);
    fe2.previousFuelEntry = fe1;
    fuelEntries.add(fe2);
    FuelEntry fe3 = new FuelEntry();
    fe3.amountRefueld = 20.1;
    fe3.totalDistance = 124572;
    fe3.dateTime = new DateTime(2019);
    fe3.previousFuelEntry = fe2;
    fuelEntries.add(fe3);
    var reversed = fuelEntries.reversed.toList();
    return reversed;
  }

  FuelEntry createFuelEntry() {
    FuelEntry fe = new FuelEntry();
    fe.amountRefueld = 35.5;
    fe.totalDistance = 123456;
    fe.dateTime = new DateTime(2017);
    return fe;
  }

  double getMeanFuelConsumption(List<FuelEntry> fuelEntries) {
    double result;
    int noOfValidFuelConsumptionValues = 0;
    double accumulatedFuelConsumptionValues = 0;
    fuelEntries.forEach((fuelEntry) {
      if (fuelEntry.consumption() != null) {
        noOfValidFuelConsumptionValues++;
        accumulatedFuelConsumptionValues += fuelEntry.consumption();
      }
      result = accumulatedFuelConsumptionValues / noOfValidFuelConsumptionValues;
    });
    return result;
  }
}
