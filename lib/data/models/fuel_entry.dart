import 'package:intl/intl.dart';

class FuelEntry {
  double amountRefueld;
  double totalDistance;
  DateTime dateTime;
  FuelEntry previousFuelEntry;

  FuelEntry(
      {this.amountRefueld,
      this.totalDistance,
      this.dateTime,
      this.previousFuelEntry});

  double distanceSinceLastRefueld() {
    double distanceSinceLastRefueld = 0;
    if (previousFuelEntry != null &&
        previousFuelEntry.totalDistance != null &&
        totalDistance != null) {
      distanceSinceLastRefueld =
          totalDistance - previousFuelEntry.totalDistance;
    }
    return distanceSinceLastRefueld;
  }

  double consumption() {
    double result;
    double distanceSinceLastRefueld = this.distanceSinceLastRefueld();
    if (distanceSinceLastRefueld != null && distanceSinceLastRefueld > 0) {
      result = amountRefueld * 10000 / distanceSinceLastRefueld;
      result = result.roundToDouble();
      result = result / 100;
    }
    return result;
  }

  String dateTimeString() {
    String formattedDate = DateFormat('kk:mm   dd.MM.yyyy').format(dateTime);
    return formattedDate;
  }
}
